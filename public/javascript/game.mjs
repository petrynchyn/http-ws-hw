const username = sessionStorage.getItem("username");
const usersContainer = document.getElementById("users-container");
const readyButton = document.getElementById("ready-button");
const disconnectButton = document.getElementById("disconnect-button");
const secondsBeforeStart = document.getElementById("seconds-before-start");
const secondsLeft = document.getElementById("seconds-left");
const timeLeft = document.getElementById("time-left");
const textConteiner = document.getElementById("text");

let users;
let text;
let charPos = 0;

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

const messageLogin = data => {
  alert(data);
  sessionStorage.removeItem("username");
  window.location.replace("/login");
};

const updateUsers = data => {
  let usersTemplate = '';
  users = new Map(Object.entries(data));

  users.forEach((value, key) => {
    usersTemplate += `
      <div id=${key} class="user" style="color: ${value.ready && 'green'}">
        &#9787; ${key} ${key === username ? ' (You)': ''}
        <div class="progress-cont">
          <div class="progress" style="width: ${value.progress}%; background-color: ${value.progress == 100 ? 'lime' : 'limegreen'};"></div>
        </div>
      </div>`;
  });
  
  usersContainer.innerHTML = usersTemplate;

  readyButton.innerText = users.get(username).ready ? 'Not ready': 'Ready' ;
};

const formatText = () => {
  textConteiner.innerHTML = `<span>${text.slice(0, charPos)}</span><span>${text.charAt(charPos)}</span>${text.slice(charPos + 1)}`
};

const handlerKeyDown = event => {
  if (event.key === text.charAt(charPos)) {
    socket.emit('PROGRESS', charPos);
    charPos++;
    formatText();
  }
};

const gameOver = (winners) => {

  document.removeEventListener('keydown', handlerKeyDown);

  timeLeft.style.visibility = 'hidden';

  setTimeout(() => {
    alert(winners);

    disconnectButton.style.visibility = 'visible';

    timeLeft.classList.add("display-none");
    timeLeft.style.visibility = 'visible';

    textConteiner.classList.add("display-none");

    readyButton.classList.remove("display-none");
  }, 100);

};


const beforeStartGame = textId => {

  disconnectButton.style.visibility = 'hidden';
  readyButton.classList.add("display-none");
  secondsBeforeStart.classList.remove("display-none");

  fetch(`/game/texts/${textId}`)
    .then(res => res.text()
      .then(data => {
        text = data;
        textConteiner.innerText = text;
      })
    );
};

const secondsBeforeStartGame = seconds => {
  secondsBeforeStart.innerText = seconds;
};

const startGame = () => {
  charPos = 0;
  secondsBeforeStart.classList.add("display-none");
  document.addEventListener('keydown', handlerKeyDown)
  timeLeft.classList.remove("display-none");
  textConteiner.classList.remove("display-none");
};

const secondsForGame = seconds => {
  secondsLeft.innerText = seconds;
};

disconnectButton.onclick = () => {
  socket.disconnect();
  sessionStorage.removeItem("username");
  window.location.replace("/login");
};

readyButton.onclick = () => socket.emit('PLAYER_READY');

socket.on("MESSAGE_LOGIN", messageLogin);
socket.on("UPDATE_USERS", updateUsers);
socket.on("BEFORE_START_GAME", beforeStartGame);
socket.on("SECONDS_BEFORE_START_GAME", secondsBeforeStartGame);
socket.on("START_GAME", startGame);
socket.on("SECONDS_FOR_GAME", secondsForGame);
socket.on("GAME_OVER", gameOver);