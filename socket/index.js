import * as config from "./config";
import { texts } from "../data";

const users = new Map();
const winners = new Map();
let textsIndx;
let isGameGoing = null;
let timerId;
let startTime;

const timer = (sec, eventName, callback, socket, ...param) => {
  emitAll(socket, eventName, sec);

  if (sec)
    timerId = setTimeout(timer, 1000, --sec, eventName, callback, socket, ...param);
  else
    callback(socket, ...param);
};

const startGame = socket => {
  emitAll(socket, "START_GAME");

  isGameGoing = true;

  timer(config.SECONDS_FOR_GAME, 'SECONDS_FOR_GAME', checkGameOver, socket, true);

  startTime = Date.now();
}

const checkPlayersReady = (socket) => {
  if (isGameGoing) return;

  let ready = true;
  users.forEach(value => {
    if (ready && !value.ready) ready = false;
  })

  if (ready) {
    textsIndx = randomIndx(texts.length);

    isGameGoing = false;

    emitAll(socket, "BEFORE_START_GAME", textsIndx);

    timer(config.SECONDS_TIMER_BEFORE_START_GAME, 'SECONDS_BEFORE_START_GAME', startGame, socket);
  }
}

const checkGameOver = (socket, timeOut) => {
  if (timeOut) isGameGoing = false;

  if (isGameGoing) {
  
    let isPlayersFinished = true;

    users.forEach(value => {
      if (isPlayersFinished && value.progress !== 100) isPlayersFinished = false;
    })
    
    if (isPlayersFinished)
      isGameGoing = false;
    else
      return;
  }

  clearTimeout(timerId);

  isGameGoing = null;

  users.forEach(value => {
    value.ready = false;
    value.progress = 0;
  });
  emitAll(socket, "UPDATE_USERS", Object.fromEntries(users));
  
  let winnersStr = 'Winners:';
  winners.forEach((value, key) => {
    winnersStr += `\n ${key} - ${value} c`;
  });
  winners.clear();
  emitAll(socket, "GAME_OVER", winnersStr);
 
}

const emitAll = (socket, eventName, ...args) => {
  socket.emit(eventName, ...args);
  socket.broadcast.emit(eventName, ...args);
}

function randomIndx(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

export default io => {
  io.on("connection", async socket => {
    const username = socket.handshake.query.username;

    if (users.size >= config.MAXIMUM_USERS_FOR_ONE_ROOM) {
      
      socket.emit("MESSAGE_LOGIN", "Too many users");
      socket.disconnect();
      
    } else if (users.has(username)) {

      socket.emit("MESSAGE_LOGIN", "User already exists");
      socket.disconnect();

    } else if (isGameGoing !== null) {

      socket.emit("MESSAGE_LOGIN", "Game already running, try again later");
      socket.disconnect();

    } else {

      users.set(username, { ready: false, progress: 0 });
      console.log(`User ${username} connected.`);

      emitAll(socket, "UPDATE_USERS", Object.fromEntries(users));
      
    }

    socket.on('PLAYER_READY', () => {
      const user = users.get(username);
      user.ready = !user.ready;
      users.set(username, user);

      emitAll(socket, "UPDATE_USERS", Object.fromEntries(users));

      checkPlayersReady(socket);
    });

    socket.on('PROGRESS', charPos => {
      if (!isGameGoing) return;

      const user = users.get(username);
      user.progress = Math.floor((charPos + 1) * 100 / texts[textsIndx].length);
      users.set(username, user);

      emitAll(socket, "UPDATE_USERS", Object.fromEntries(users));

      if (user.progress === 100) {
        winners.set(username, (Date.now() - startTime) / 1000);
        checkGameOver(socket);
      }
    });

    socket.on('disconnect', reason => {
      users.delete(username);
      winners.delete(username);
      socket.broadcast.emit("UPDATE_USERS", Object.fromEntries(users));
      console.log(`User ${username} disconnected.`);

      if (isGameGoing) {
        checkGameOver(socket);
      } else if (isGameGoing === null) {
        checkPlayersReady(socket);
      }

      if (!users.size) {
        isGameGoing = null;
        clearTimeout(timerId);
        winners.clear();
      }
      
    });
  });
};
